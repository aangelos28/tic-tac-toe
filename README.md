# NxN Tic-Tac-Toe
This is a simple implementation of NxN Tic-Tac-Toe in C#, .NET Core

## Features
* NxN board drawing
* Two human players
* Input validation