﻿using System;
using static TicTacToe.Board;

namespace TicTacToe
{
    internal struct Move
    {
        public int Row;
        public int Column;

        public Move(int row, int column)
        {
            Row = row;
            Column = column;
        }
    }

    internal class Player : IEquatable<Player>
    {
        // Properties
        public int Id { get; protected set; }
        public Symbol AssignedSymbol { get; protected set; }
        public Board AssignedBoard { get; }

        /// <summary>
        /// Parameterized constructor
        /// </summary>
        /// <param name="id"> The id of the player (0, 1) </param>
        /// <param name="assignedSymbol"> The symbol to assign to the player </param>
        /// <param name="assignedBoard"> The board assigned to the player </param>
        public Player(int id, Symbol assignedSymbol, Board assignedBoard)
        {
            Id = id;
            AssignedSymbol = assignedSymbol;
            AssignedBoard = assignedBoard;
        }

        /// <summary>
        /// Change the value of the cell under row and col to be the player's assigned symbol
        /// </summary>
        /// <param name="board"> The board to make the move on </param>
        /// <param name="move"> Struct representing the move to make </param>
        public void MakeMove(Move move)
        {
            AssignedBoard[move.Row, move.Column] = AssignedSymbol;
        }


        public bool Equals(Player other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return Id == other.Id && AssignedSymbol == other.AssignedSymbol && Equals(AssignedBoard, other.AssignedBoard);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;

            return Equals((Player)obj);
        }

        public override int GetHashCode()
        {
            unchecked {
                var hashCode = Id;
                hashCode = (hashCode * 397) ^ (int)AssignedSymbol;
                hashCode = (hashCode * 397) ^ (AssignedBoard != null ? AssignedBoard.GetHashCode() : 0);

                return hashCode;
            }
        }

        public static bool operator ==(Player left, Player right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Player left, Player right)
        {
            return !Equals(left, right);
        }
    }
}
