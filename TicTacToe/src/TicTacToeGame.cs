﻿using System;

namespace TicTacToe
{
    internal sealed class TicTacToeGame
    {
        // Properties
        public Board Board { get; private set; }

        public Player Player1 { get; private set; }
        public Player Player2 { get; private set; }

        public Referee Referee { get; private set; }

        /// <summary>
        /// Default constructor.
        /// Create 3x3 Tic-Tac-Toe board with 2 human players.
        /// </summary>
        public TicTacToeGame()
        {
            Board = new Board(3, 3);

            Player1 = new Player(0, Board.Symbol.X, Board);
            Player2 = new Player(1, Board.Symbol.O, Board);

            Referee = new Referee(Board);
        }

        /// <summary>
        /// Parameterized constructor.
        /// Create Tic-Tac-Toe board of specified width and height with 2 human players.
        /// </summary>
        /// <param name="boardWidth"> The width of the Tic-Tac-Toe board </param>
        /// <param name="boardHeight"> The height of the Tic-Tac-Toe board </param>
        public TicTacToeGame(int boardWidth, int boardHeight)
        {
            Board = new Board(boardWidth, boardHeight);

            Player1 = new Player(0, Board.Symbol.X, Board);
            Player2 = new Player(1, Board.Symbol.O, Board);

            Referee = new Referee(Board);
        }

        /// <summary>
        /// Parameterized constructor.
        /// Create custom Tic-Tac-Toe board with 2 players.
        /// </summary>
        /// <param name="board"> The board to use </param>
        /// <param name="player1"> The first player </param>
        /// <param name="player2"> The second player </param>
        /// <param name="referee"> The game referee </param>
        public TicTacToeGame(Board board, Player player1, Player player2, Referee referee)
        {
            Board = board;

            Player1 = player1;
            Player2 = player2;

            Referee = referee;
        }

        /// <summary>
        /// Play a round of Tic-Tac-Toe.
        /// </summary>
        public void Play()
        {
            string endMessage = string.Empty;

            int currentPlayerId = 0;

            bool gameEnd = false;

            // Play game
            while (!gameEnd) {

                bool roundEnd = false;

                // Play round
                while (!roundEnd) {

                    Console.Write(Board.ToString());

                    // Get current player
                    Player currentPlayer;
                    switch (currentPlayerId) {
                        case 0:
                            currentPlayer = Player1;
                            break;
                        case 1:
                            currentPlayer = Player2;
                            break;
                        default:
                            throw new ApplicationException("Invalid player.");
                    }

                    var currentPlayerMove = GetPlayerMove(currentPlayer);
                    currentPlayer.MakeMove(currentPlayerMove);

                    // Check if this is the round's last move
                    roundEnd = Referee.CheckEndingMove(currentPlayer, currentPlayerMove, out endMessage);

                    // Switch to other player ID
                    currentPlayerId = 1 - currentPlayerId;

                    Console.Clear();
                }

                // Print final board state
                Console.Write(Board.ToString());

                Console.Write("\n\n");
                Console.WriteLine(endMessage);

                // New round?
                Console.Write("\nPlay another? (y/n) ");
                char response = Console.ReadLine()[0];

                if (response == 'y' || response == 'Y') {
                    roundEnd = false;
                    Reset();
                    Console.Clear();
                    continue;
                }

                gameEnd = true;
            }
        }

        /// <summary>
        /// Reset the game. Reset the board.
        /// </summary>
        public void Reset()
        {
            Board.Reset();
        }

        /// <summary>
        /// Ask the current player to enter a move
        /// </summary>
        /// <param name="player"> The current player </param>
        /// <returns> The selected move </returns>
        private Move GetPlayerMove(Player player)
        {
            Move playerMove = new Move();

            while (true) { 

                Console.Write($"\n\nPlayer {player.Id + 1}, enter a position [1-{Board.TotalRows * Board.TotalColumns}]: ");
                string result = Console.ReadLine();
                
                // Ensure proper input format
                if (!int.TryParse(result, out int selectedPosition)) {
                    Console.WriteLine("Invalid input format.");
                    continue;
                }

                if (selectedPosition <= 0 || selectedPosition > Board.TotalRows * Board.TotalColumns) {
                    Console.WriteLine("Invalid move. Outside board bounds.");
                    continue;
                }

                // Translate selected position as a move
                playerMove.Row = (selectedPosition - 1) / Board.TotalRows;
                playerMove.Column = (selectedPosition - 1) % Board.TotalColumns;

                // Ensure that the selected position is not occupied
                if (!Referee.ValidMove(player, playerMove)) {
                    Console.WriteLine("Invalid move. Position occupied.");
                    continue;
                }

                break;
            }

            return playerMove;
        }
    }
}
