﻿using System;
using System.Text;

namespace TicTacToe
{
    internal sealed class Board : IEquatable<Board>
    {
        public enum Symbol
        {
            X = 'X',
            O = 'O',
            N = 0
        };

        // Fields
        private readonly Symbol[,] grid;
        private readonly int cellWidth;

        // Properties
        public int OccupiedCells { get; private set; }

        public int TotalRows { get; }
        public int TotalColumns { get; }

        /// <summary>
        /// Parameterized constructor.
        /// Create board of size width * height.
        /// <param name="width"> The width of the board (number of columns) </param>
        /// <param name="height"> The height of the board (number of rows) </param>
        /// </summary>
        public Board(int width, int height)
        {
            grid = new Symbol[width,height];
            cellWidth = (width * height).ToString().Length + 2;

            // Initialize grid
            Reset();

            TotalRows = grid.GetLength(0);
            TotalColumns = grid.GetLength(1);
        }

        /// <summary>
        /// Board indexer. Gives access to the underlying grid.
        /// </summary>
        /// <param name="row"> The row of the cell to access </param>
        /// <param name="col"> The column of the cell to access </param>
        /// <returns></returns>
        public Symbol this[int row, int col]
        {
            get => grid[row, col];
            set {
                grid[row, col] = value;
                ++OccupiedCells;
            }
        }

        /// <summary>
        /// Get whether the board is full.
        /// </summary>
        /// <returns> True if board is full, false if not </returns>
        public bool IsFull()
        {
            return OccupiedCells == grid.GetLength(0) * grid.GetLength(1);
        }

        /// <summary>
        /// Reset the grid to contain neutral symbols.
        /// </summary>
        public void Reset()
        {
            // Reset grid
            for (var row = 0; row < grid.GetLength(0); ++row) {
                for (var col = 0; col < grid.GetLength(1); ++col) {
                    grid[row, col] = Symbol.N;
                }
            }

            OccupiedCells = 0;
        }

        /// <summary>
        /// Get the contents of the cell on the specified row and column
        /// </summary>
        /// <param name="row"> The row of the cell </param>
        /// <param name="col"> The column of the cell </param>
        /// <returns> The content of the cell </returns>
        private dynamic GetCellContents(int row, int col)
        {
            if (grid[row, col] == Symbol.N) {
                return col + row * grid.GetLength(1) + 1;
            }

            return (char)grid[row, col];
        }

        /// <summary>
        /// Draw the board dynamically.
        /// </summary>
        /// <returns> String representation of the board </returns>
        public override string ToString()
        {
            var sep = new string('-', cellWidth);

            var res = new StringBuilder();

            var currentCell = 1;
 
            // Draw every row
            for (var row = 0; row < TotalRows ; ++row) {

                // Draw separator
                for (var col = 0; col < grid.GetLength(1); ++col) {
                    res.Append("+" + sep);
                }

                res.Append("+\n");

                var line = new StringBuilder(new string(' ', cellWidth + 1))
                {
                    [0] = '|'
                };

                // Draw line
                for (var col = 0; col < TotalColumns; ++col) {

                    var cellContents = GetCellContents(row, col);

                    if (cellContents is char) {
                        line[line.Length / 2] = cellContents;
                    }
                    else {
                        var digits = cellContents.ToString();
                        var totalDigits = digits.Length;

                        var initialPos = line.Length / 2;

                        for (int i = initialPos, k = 0; i < initialPos + totalDigits; ++i, ++k) {
                            line[i] = digits[k];
                        }
                    }
                    
                    res.Append(line);
                    ++currentCell;

                    // Reset line
                    for (var i = 1; i < line.Length; ++i) {
                        line[i] = ' ';
                    }
                }

                res.Append("|\n");
            }

            // Draw final column
            for (var col = 0; col < TotalColumns; ++col) {
                res.Append("+" + sep);
            }
            res.Append("+");

            return res.ToString();
        }

        public bool Equals(Board other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return grid.Equals(other.grid) && OccupiedCells == other.OccupiedCells;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;

            return obj is Board other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked {
                return (grid.GetHashCode() * 397) ^ OccupiedCells;
            }
        }

        public static bool operator ==(Board left, Board right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Board left, Board right)
        {
            return !Equals(left, right);
        }
    }
}
