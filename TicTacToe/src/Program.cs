﻿namespace TicTacToe
{
    class Program
    {
        private static void Main(string[] args)
        {
            var ticTacToe = new TicTacToeGame(3, 3);

            ticTacToe.Play();
        }
    }
}
