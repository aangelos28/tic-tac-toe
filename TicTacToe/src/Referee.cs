﻿using System;

namespace TicTacToe
{
    internal sealed class Referee : IEquatable<Referee>
    {
        // Fields
        private const string Player1WonMessage = "Player 1 is victorious!";
        private const string Player2WonMessage = "Player 2 is victorious!";
        private const string StalemateMessage = "Tie!";

        // Properties
        public Board AssignedBoard { get; }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="assignedBoard"> The board the referee is assigned to </param>
        public Referee(Board assignedBoard)
        {
            AssignedBoard = assignedBoard;
        }

        /// <summary>
        /// Determine if the specified move at the cell under row and col is valid.
        /// </summary>
        /// <param name="player"> The player making the move </param>
        /// <param name="move"> Struct representing the move to make </param>
        /// <returns> Whether the move is valid </returns>
        public bool ValidMove(Player player, Move move)
        {
            return AssignedBoard[move.Row, move.Column] == Board.Symbol.N;
        }

        /// <summary>
        /// Determine whether the move ends the game.
        /// </summary>
        /// <param name="player"> The player making the move </param>
        /// <param name="move"> Struct representing the move to make </param>
        /// <param name="endMessage"> The message to show at the end of the game </param>
        /// <returns> Whether the move ends the game </returns>
        public bool CheckEndingMove(Player player, Move move, out string endMessage)
        {
            // Check if the board is full
            if (AssignedBoard.IsFull()) {
                endMessage = StalemateMessage;
                return true;
            }

            // Get board dimensions
            var maxRow = AssignedBoard.TotalRows;
            var maxCol = AssignedBoard.TotalColumns;

            // Check row
            bool endingMove = AssignedBoard[move.Row, 0] == player.AssignedSymbol;
            for (var col = 1; col < maxCol; ++col) {
                endingMove = endingMove && (AssignedBoard[move.Row, col] == player.AssignedSymbol);
            }

            if (endingMove) {
                endMessage = player.Id == 0 ? Player1WonMessage : Player2WonMessage;
                return true;
            }

            // Check column
            endingMove = AssignedBoard[0, move.Column] == player.AssignedSymbol;
            for (var row = 1; row < maxRow; ++row) {
                endingMove = endingMove && (AssignedBoard[row, move.Column] == player.AssignedSymbol);
            }

            if (endingMove) {
                endMessage = player.Id == 0 ? Player1WonMessage : Player2WonMessage;
                return true;
            }

            // Check negatively-sloped diagonal
            endingMove = AssignedBoard[0, 0] == player.AssignedSymbol;
            for (var i = 1; i < maxCol; ++i) {
                endingMove = endingMove && (AssignedBoard[i, i] == player.AssignedSymbol);
            }

            if (endingMove) {
                endMessage = player.Id == 0 ? Player1WonMessage : Player2WonMessage;
                return true;
            }

            // Check positively-sloped diagonal
            endingMove = AssignedBoard[maxRow - 1, 0] == player.AssignedSymbol;
            for (var i = 0; i < maxCol; ++i) {
                endingMove = endingMove && (AssignedBoard[maxRow - 1 - i, i] == player.AssignedSymbol);
            }

            if (endingMove) {
                endMessage = player.Id == 0 ? Player1WonMessage : Player2WonMessage;
                return true;
            }

            endMessage = string.Empty;
            return false;
        }

        public bool Equals(Referee other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return Equals(AssignedBoard, other.AssignedBoard);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;

            return obj is Referee other && Equals(other);
        }

        public override int GetHashCode()
        {
            return (AssignedBoard != null ? AssignedBoard.GetHashCode() : 0);
        }

        public static bool operator ==(Referee left, Referee right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Referee left, Referee right)
        {
            return !Equals(left, right);
        }
    }
}
